(function() {
  'use strict';

  angular.module('app.directus', [
    'app.core',
    'app.widgets'
  ]);

})();
