(function() {
  'use strict';

  angular
    .module('app.directus')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'directus',
        config: {
          url: '/directus',
          templateUrl: 'app/directus/directus.html',
          controller: 'DirectusController',
          controllerAs: 'vm',
          title: 'Directus',
          settings: {
            nav: 2,
            content: '<i class="fa fa-github"></i> Directus'
          }
        }
      }
    ];
  }
})();
