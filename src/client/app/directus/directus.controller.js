(function () {
  'use strict';

  angular
    .module('app.directus')
    .controller('DirectusController', DirectusController);

  DirectusController.$inject = ['logger', 'directusRepoService', '$sce'];
  /* @ngInject */
  function DirectusController(logger, directusRepoService, $sce) {
    var vm = this;
    vm.title = 'Directus';
    vm.articles = [];
    vm.articlesAsJSON = '';
    vm.article = {};
    vm.youtubeVideo = {};
    vm.companyValues = [];

    vm.trustAsHtml = trustAsHtml;

    activate();

    function activate() {
      logger.info('Activated Directus View');
      loadData();
    }

    function loadData() {
      getAllArticles();
      getArticleById('1');
      getYoutubeVideo();
      getCompanyValues();
    }

    function getAllArticles() {
      directusRepoService.getArticles()
        .then(setArticles)
        .catch(fail);
    }

    function setArticles(articles) {
      vm.articles = articles;
      vm.articlesAsJSON = JSON.stringify(articles, null, 2);
      return vm.articles;
    }

    function getArticleById(id) {
      directusRepoService.getArticleById(id)
        .then(setArticle)
        .catch(fail);
    }

    function setArticle(article) {
      vm.article = article;
      return vm.article;
    }

    function getYoutubeVideo() {
      directusRepoService.getYoutubeVideos()
        .then(setYoutubeVideo)
        .catch(fail);
    }

    function setYoutubeVideo(videos) {
      vm.youtubeVideo = videos[0];
      return vm.youtubeVideo;
    }

    function getCompanyValues() {
      directusRepoService.getCompanyValues()
        .then(setCompanyValues)
        .catch(fail);
    }

    function setCompanyValues(companyValues) {
      vm.companyValues = companyValues.rows;
      return vm.companyValues;
    }

    function trustAsHtml(html) {
      return $sce.trustAsHtml(html);
    }

    function fail(e) {
      logger.error('Directus: Loading data failed. Make sure the Directus Vagrant is running and has some articles, youtube videos or company values.');
      return e;
    }

  }
})();
