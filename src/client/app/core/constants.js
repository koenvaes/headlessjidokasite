/* global toastr:false, moment:false */
(function () {
  'use strict';

  angular
    .module('app.core')
    .constant('toastr', toastr)
    .constant('moment', moment)
    .constant('CONFIG', {
      HOST: 'http://192.168.33.6',
      API_URL: 'http://192.168.33.6' + '/api/1/',
      ACCESS_TOKEN: 'access_token=ucyQLuwFvzKRWejwbNjB3FhXXuUsulzH',
      FILES_URL: 'files',
      ARTICLES_URL: 'tables/articles/rows',
      COMPANY_VALUES_URL: 'tables/company_values/rows',
      CONTACT_DETAILS_URL: 'tables/contact_details/rows',
      EMPLOYEE_OFFERING_URL: 'tables/employee_offering/rows',
      MISSION_STATEMENT_URL: 'tables/mission_statement/rows',
      PARTNERS_URL: 'tables/partners/rows',
      PREMIUM_CONSULTANCY_URL: 'tables/premium_consultancy/rows',
      PROJECT_PHASES_URL: 'tables/project_phases/rows',
      PROJECT_PHASES_HEADER_URL: 'tables/project_phases_header/rows',
      PROJECT_RESCUE_URL: 'tables/project_rescue/rows'
    });
})();
