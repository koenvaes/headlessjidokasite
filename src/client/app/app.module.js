(function () {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.widgets',
    'app.site',
    'app.directus',
    'app.layout',
    'app.data'
  ]);

})();
