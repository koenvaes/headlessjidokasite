'use strict';

angular
  .module('app.data')
  .service('directusRepoService', directusRepoService);

directusRepoService.$inject = ['$resource', 'CONFIG'];

function directusRepoService($resource, CONFIG) {
  var onlyActive = '&status=1';

  return {
    getArticles: getArticles,
    getArticleById: getArticleById,
    getFiles: getFiles,
    getYoutubeVideos: getYoutubeVideos,
    getCompanyValues: getCompanyValues
  };

  function getArticles() {
    var articleResource = $resource(CONFIG.API_URL + CONFIG.ARTICLES_URL + '?' + CONFIG.ACCESS_TOKEN + onlyActive);
    return articleResource.get().$promise;
  }

  function getArticleById(articleId) {
    var articleResource = $resource(CONFIG.API_URL + CONFIG.ARTICLES_URL + '/' + articleId + '?' + CONFIG.ACCESS_TOKEN);
    return articleResource.get().$promise;
  }

  function getFiles() {
    var articleResource = $resource(CONFIG.API_URL + CONFIG.FILES_URL + '?' + CONFIG.ACCESS_TOKEN + onlyActive);
    return articleResource.get().$promise;
  }

  function getYoutubeVideos() {
    return getFiles()
      .then(function (filesObject) {
        return filesObject.rows.filter(function (file) {
          return file.type === 'embed/youtube';
        });
      });
  }

  function getCompanyValues() {
    var articleResource = $resource(CONFIG.API_URL + CONFIG.COMPANY_VALUES_URL + '?' + CONFIG.ACCESS_TOKEN + onlyActive);
    return articleResource.get().$promise;
  }
}
