'use strict';

angular
  .module('app.data')
  .service('siteRepoService', siteRepoService);

siteRepoService.$inject = ['$resource', 'CONFIG'];

function siteRepoService($resource, CONFIG) {
  var onlyActive = '&status=1';

  return {
    getFiles: getFiles,
    getCompanyValues: getCompanyValues,
    getContactDetails: getContactDetails,
    getEmployeeOfferings: getEmployeeOfferings,
    getMissionStatement: getMissionStatement,
    getPartners: getPartners,
    getPremiumConsultancy: getPremiumConsultancy,
    getProjectPhases: getProjectPhases,
    getProjectPhasesHeader: getProjectPhasesHeader,
    getProjectRescue: getProjectRescue
  };

  function getFiles() {
    var articleResource = $resource(CONFIG.API_URL + CONFIG.FILES_URL + '?' + CONFIG.ACCESS_TOKEN + onlyActive);
    return articleResource.get().$promise;
  }

  function getCompanyValues() {
    return getActiveItemsFromURL(CONFIG.COMPANY_VALUES_URL);
  }

  function getContactDetails() {
    return getActiveItemsFromURL(CONFIG.CONTACT_DETAILS_URL);
  }

  function getEmployeeOfferings() {
    return getActiveItemsFromURL(CONFIG.EMPLOYEE_OFFERING_URL);
  }

  function getMissionStatement() {
    return getActiveItemsFromURL(CONFIG.MISSION_STATEMENT_URL);
  }

  function getPartners() {
    return getActiveItemsFromURL(CONFIG.PARTNERS_URL);
  }

  function getPremiumConsultancy() {
    return getActiveItemsFromURL(CONFIG.PREMIUM_CONSULTANCY_URL);
  }

  function getProjectPhases() {
    return getActiveItemsFromURL(CONFIG.PROJECT_PHASES_URL);
  }

  function getProjectPhasesHeader() {
    return getActiveItemsFromURL(CONFIG.PROJECT_PHASES_HEADER_URL);
  }

  function getProjectRescue() {
    return getActiveItemsFromURL(CONFIG.PROJECT_RESCUE_URL);
  }

  function getActiveItemsFromURL(url) {
    var dataResource = $resource(CONFIG.API_URL + url + '?' + CONFIG.ACCESS_TOKEN + onlyActive);
    return dataResource.get().$promise;
  }
}
