/* jshint -W117, -W030 */
describe('site routes', function() {
  describe('state', function() {
    var view = 'app/site/site.html';

    beforeEach(function() {
      module('app.site', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    bard.verifyNoOutstandingHttpRequests();

    it('should map state site to url / ', function() {
      expect($state.href('site', {})).to.equal('/');
    });

    it('should map /site route to site View template', function() {
      expect($state.get('site').templateUrl).to.equal(view);
    });

    it('of site should work with $state.go', function() {
      $state.go('site');
      $rootScope.$apply();
      expect($state.is('site'));
    });
  });
});
