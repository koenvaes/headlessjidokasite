(function() {
  'use strict';

  angular
    .module('app.site')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'site',
        config: {
          url: '/',
          templateUrl: 'app/site/site.html',
          controller: 'SiteController',
          controllerAs: 'vm',
          title: 'site',
          settings: {
            nav: 1,
            content: '<i class="fa fa-globe"></i> Site'
          }
        }
      }
    ];
  }
})();
