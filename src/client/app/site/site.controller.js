(function () {
  'use strict';

  angular
    .module('app.site')
    .controller('SiteController', SiteController);

  SiteController.$inject = ['logger', 'siteRepoService', '$sce', 'CONFIG'];
  /* @ngInject */
  function SiteController(logger, siteRepoService, $sce, CONFIG) {
    var vm = this;
    vm.title = 'Site';
    vm.companyValues = [];
    vm.contactDetails = [];
    vm.employeeOfferings = [];
    vm.missionStatement = [];
    vm.partners = [];
    vm.premiumConsultancy = [];
    vm.projectPhasesHeader = [];
    vm.projectPhases = [];
    vm.projectRescue = [];

    vm.trustAsHtml = trustAsHtml;

    vm.host = CONFIG.HOST;

    activate();

    function activate() {
      loadData();
    }

    function loadData() {
      getCompanyValues();
      getContactDetails();
      getEmployeeOfferings();
      getMissionStatement();
      getPartners();
      getPremiumConsultancy();
      getProjectPhases();
      getProjectPhasesHeader();
      getProjectRescue();
    }

    function getCompanyValues() {
      siteRepoService.getCompanyValues()
        .then(setCompanyValues)
        .catch(fail);

      function setCompanyValues(companyValues) {
        vm.companyValues = companyValues.rows;
        return vm.companyValues;
      }
    }

    function getContactDetails() {
      siteRepoService.getContactDetails()
        .then(setContactDetails)
        .catch(fail);

      function setContactDetails(contactDetails) {
        vm.contactDetails = contactDetails.rows;
        return vm.contactDetails;
      }
    }

    function getEmployeeOfferings() {
      siteRepoService.getEmployeeOfferings()
        .then(setEmployeeOfferings)
        .catch(fail);

      function setEmployeeOfferings(employeeOfferings) {
        vm.employeeOfferings = employeeOfferings.rows;
        return vm.employeeOfferings;
      }
    }

    function getMissionStatement() {
      siteRepoService.getMissionStatement()
        .then(setMissionStatement)
        .catch(fail);

      function setMissionStatement(missionStatement) {
        vm.missionStatement = missionStatement.rows;
        return vm.missionStatement;
      }
    }

    function getPartners() {
      siteRepoService.getPartners()
        .then(setPartners)
        .catch(fail);

      function setPartners(partners) {
        vm.partners = partners.rows;
        return vm.partners;
      }
    }

    function getPremiumConsultancy() {
      siteRepoService.getPremiumConsultancy()
        .then(setPremiumConsultancy)
        .catch(fail);

      function setPremiumConsultancy(premiumConsultancy) {
        vm.premiumConsultancy = premiumConsultancy.rows;
        return vm.premiumConsultancy;
      }
    }

    function getProjectPhases() {
      siteRepoService.getProjectPhases()
        .then(setProjectPhases)
        .catch(fail);

      function setProjectPhases(projectPhases) {
        vm.projectPhases = projectPhases.rows;
        return vm.projectPhases;
      }
    }

    function getProjectPhasesHeader() {
      siteRepoService.getProjectPhasesHeader()
        .then(setProjectPhasesHeader)
        .catch(fail);

      function setProjectPhasesHeader(projectPhasesHeader) {
        vm.projectPhasesHeader = projectPhasesHeader.rows;
        return vm.projectPhasesHeader;
      }
    }

    function getProjectRescue() {
      siteRepoService.getProjectRescue()
        .then(setProjectRescue)
        .catch(fail);

      function setProjectRescue(projectRescue) {
        vm.projectRescue = projectRescue.rows;
        return vm.projectRescue;
      }
    }

    function trustAsHtml(html) {
      return $sce.trustAsHtml(html);
    }

    function fail(e) {
      logger.error('Directus: Loading data failed. Make sure the Directus Vagrant is running and has some data.');
      return e;
    }
  }
})();
