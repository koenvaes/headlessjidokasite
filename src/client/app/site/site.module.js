(function() {
  'use strict';

  angular.module('app.site', [
    'app.core',
    'app.widgets'
  ]);
})();
