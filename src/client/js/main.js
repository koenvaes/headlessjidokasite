(function($) {
  "use strict";
  /* =================================
      Loader
  =================================== */
  jQuery(window).load(function() {
    jQuery(".status").fadeOut();
    jQuery(".preloader").delay(1000).fadeOut("slow");
  });

  var revapi;

   revapi = jQuery('.tp-banner').revolution(
    {
      delay:9000,
      startwidth:1170,
      startheight:875,
      navigationStyle:"round",
      navigationType:"none"
    });

  //================================= Totop  ===================================//
  $().UItoTop({
      scrollSpeed:500,
    easingType:'linear'
  });




  //========================== Nav Responsive ==================================//
  $('#menu').tinyNav({
      active: 'selected'
  });


  //========================== Smooth Scroll ==================================//
  smoothScroll.init({
      speed: 500, // Integer. How fast to complete the scroll in milliseconds
      easing: 'easeInOutQuad', // Easing pattern to use
      updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
      offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
      callbackBefore: function ( toggle, anchor ) {}, // Function to run before scrolling
      callbackAfter: function ( toggle, anchor ) {} // Function to run after scrolling
  });


  //============================= Nav Superfish ===============================//
  $('ul.sf-menu').superfish();


  //============================== Top Menu ===================================//
  $(".menu-logo").sticky({topSpacing:0});


  //============================== Sliders ===================================//
  $(".rslides").responsiveSlides({
       speed: 900
    });

  $('.flexslider').flexslider({
      pauseOnHover: true,
      direction: "horizontal",
  });


   //===================================Twitter  ==============================//
  $(".twitter").tweet({
      modpath: 'twitter/index.php',
      username: "envato",
      count: 5,
      loading_text: "Loading tweets...",
   });


  //============================== Gallery Carousel ============================//
  $(".team").owlCarousel({
      autoPlay: 4000,
      items : 3,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,2],
      itemsTablet : [768, 2],
      itemsMobile : [479, 1],
      stopOnHover : true
   });


  //=============================== Items Gallery ======================================//
  $(".items-gallery-carousel").owlCarousel({
      autoPlay: 3000,
      items : 4,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,3],
      itemsTablet   : [768,2],
      itemsTabletSmall : [560,2],
      itemsMobile : [400,1],
      stopOnHover : true
   });

  //=============================== Items blog ======================================//
  $(".items-blog").owlCarousel({
      autoPlay: 3000,
      items : 1,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet   : [768,1],
      itemsTabletSmall : [560,1],
      itemsMobile : [400,1],
      stopOnHover : true
   });

  //=============================== Items Gallery ======================================//
  $(".blog").owlCarousel({
      autoPlay: 1500,
      items : 3,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [980,2],
      itemsTablet   : [768,2],
      itemsTabletSmall : [600,1],
      itemsMobile : [400,1],
      stopOnHover : true
   });

  //=============================== Twitter List ======================================//
  $(".tweet_list").owlCarousel({
      navigation : false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem: true
   });

   //=============================== Twitter List ======================================//
  $(".testimonials").owlCarousel({
      navigation : false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem: true
   });

  //=============================== Sponsor List ======================================//
  $(".list-sponsors").owlCarousel({
      autoPlay: 4000,
      items : 4,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,3],
      itemsTablet   : [768,3],
      itemsTabletSmall : [560,3],
      itemsMobile : [400,1],
      stopOnHover : true
   });

  //=============================== Sponsor List ======================================//
  $("#pricing-carousel").owlCarousel({
      autoPlay: 4000,
      items : 3,
      pagination: true,
      navigation: false,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,3],
      itemsTablet   : [768,3],
      itemsTabletSmall : [560,3],
      itemsMobile : [400,3],
      stopOnHover : true
   });

  //=================================== Slide Team  ================================//
  $("#slide-team").owlCarousel({
      items : 1,
      autoPlay: false,
      navigation : true,
      autoHeight : true,
      slideSpeed : 400,
      singleItem: true,
      pagination : false
  });

  //============================ Lightbox ====================================//
  $("a.fancybox").fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
  });

    $(window).load(function(){
        tinymce.init({
            selector:'div.editable',
            inline: true
        });
    });

   //=================================== Portfolio Filters  ==============================//
  $(window).load(function(){
     var $container = $('.portfolioContainer');
     $container.isotope({
      filter: '*',
              animationOptions: {
              duration: 750,
              easing: 'linear',
              queue: false
            }
     });
    $('.portfolioFilter a').click(function(){
      $('.portfolioFilter .current').removeClass('current');
      $(this).addClass('current');
       var selector = $(this).attr('data-filter');
       $container.isotope({
        filter: selector,
               animationOptions: {
               duration: 750,
               easing: 'linear',
               queue: false
             }
        });
       return false;
      });
   });


  //================================= Subtmit Form  =================================//
  $('.form-contact').submit(function(event) {
      event.preventDefault();
      var url = $(this).attr('action');
      var datos = $(this).serialize();
      $.get(url, datos, function(resultado) {
        $('.result').html(resultado);
      });
  });


  //=================================== Google Map  ==============================//
  /*
    Map Settings
    Find the Latitude and Longitude of your address:
    - http://universimmedia.pagesperso-orange.fr/geo/loc.htm
    - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/
  */

  // Map Markers
  var mapMarkers = [{
    address: "Vlasmeer 5, 2400 Mol",
    html: "<strong>Jidoka HQ</strong><br>Vlasmeer 5, 2400 Mol<br>Innotek-technologiehuis mol",
    icon: {
      image: "img/pin.png",
      iconsize: [26, 46],
      iconanchor: [12, 46]
    }
  }];

  // Map Initial Location
  var initLatitude = 51.1981287;
  var initLongitude = 5.0375386;

  // Map Extended Settings
  var mapSettings = {
    controls: {
      panControl: true,
      zoomControl: true,
      mapTypeControl: true,
      scaleControl: true,
      streetViewControl: true,
      overviewMapControl: true
    },
    scrollwheel: false,
    markers: mapMarkers,
    latitude: initLatitude,
    longitude: initLongitude,
    zoom: 8
  };

  $("#map").gMap(mapSettings);


  //================================== Grid Slider ====================================//
    $('#ri-grid').gridrotator({

      // number of rows
      rows : 2,

      // number of columns
      columns : 6,

      // rows/columns for different screen widths
      // i.e. w768 is for screens smaller than 768 pixels

      w1024 : {
        rows : 2,
        columns : 4
      },

      // step: number of items that are replaced at the same time
      // random || [some number]
      // note: for performance issues, the number should not be > options.maxStep
      step : 'random',
      maxStep : 1,

      // prevent user to click the items
      preventClick : true,

      // animation type
      // showHide || fadeInOut || slideLeft ||
      // slideRight || slideTop || slideBottom ||
      // rotateLeft || rotateRight || rotateTop ||
      // rotateBottom || scale || rotate3d ||
      // rotateLeftScale || rotateRightScale ||
      // rotateTopScale || rotateBottomScale || random
      animType : 'slideTop',

      // animation speed
      animSpeed : 350,

      // animation easings
      animEasingOut : 'linear',
      animEasingIn : 'linear',

      // the item(s) will be replaced every 3 seconds
      // note: for performance issues, the time "can't" be < 300 ms
      interval : 2500,
      // if false the animations will not start
      // use false if onhover is true for example
      slideshow : true,
      // if true the items will switch when hovered
      onhover : false,
      // ids of elements that shouldn't change
      nochange : []

    });
  //=============================== Grid Slider 404page ====================================//
    $( '#ri-grid-full' ).gridrotator( {
          rows : 2,
          columns : 5,
          maxStep : 2,
          interval : 2000,
          w1024 : { rows : 4, columns : 3 },
          w768 : {rows : 4,columns : 3 },
          w480 : {rows : 4,columns : 3 },
    } );


   //================================== Text Rotator ====================================//
   $('.tlt,.tlt-2').textillate({
      // the default selector to use when detecting multiple texts to animate
      selector: '.texts',

      // enable looping
      loop: true,

      // sets the minimum display time for each text before it is replaced
      minDisplayTime: 2000,

      // sets the initial delay before starting the animation
      // (note that depending on the in effect you may need to manually apply
      // visibility: hidden to the element before running this plugin)
      initialDelay: 0,

      // set whether or not to automatically start animating
      autoStart: true,

      // custom set of 'in' effects. This effects whether or not the
      // character is shown/hidden before or after an animation
      inEffects: [],

      // custom set of 'out' effects
      outEffects: [ 'hinge' ],

      // in animation settings
      in: {
        // set the effect name
        effect: 'fadeInUp',

        // set the delay factor applied to each consecutive character
        delayScale: 1.5,

        // set the delay between each character
        delay: 50,

        // set to true to animate all the characters at the same time
        sync: true,

        // randomize the character sequence
        // (note that shuffle doesn't make sense with sync = true)
        shuffle: false,

        // reverse the character sequence
        // (note that reverse doesn't make sense with sync = true)
        reverse: false,

        // callback that executes once the animation has finished
        callback: function () {}
      },

      // out animation settings.
      out: {
        effect: 'fadeOutDown',
        delayScale: 1.5,
        delay: 50,
        sync: true,
        shuffle: false,
        reverse: false,
        callback: function () {}
      },

      // callback that executes once textillate has finished
      callback: function () {}
    });


  //=================================== Accordion  =================================//
  $('.accordion-container').hide();
  $('.accordion-trigger:first').addClass('active-accordion').next().show();
  $('.accordion-trigger').click(function(){
    if( $(this).next().is(':hidden') ) {
      $('.accordion-trigger').removeClass('active-accordion').next().slideUp();
      $(this).toggleClass('active-accordion').next().slideDown();
    }
    return false;
  });


   //=========================== Hover Effect Animations ============================//
  $('.skills a, .social li,  .list-sponsors li').hover(function() {
    $(this).toggleClass('animated rubberBand');
  });


  //=========================== Effect Animations Scroll ============================//
  $(window).scroll(function() {

    $('.skills i, .list-sponsors li').each(function(){
      var imagePos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow+600) {
          $(this).addClass("animated bounceIn").css("opacity", "1");
      }
    });

    $('.effect').each(function(){
      var imagePos = $(this).offset().top;

      var topOfWindow = $(window).scrollTop();
        if (imagePos < topOfWindow+700) {
          $(this).addClass("animated fadeInUp").css("opacity", "1");
        }
    });

  });


})(jQuery);


//=================================== Background Moving ====================================//
   // speed in milliseconds
   var scrollSpeed = 70;
   // set the default position
   var current = 0;
   // set the direction
   var direction = 'h';
   function bgscroll(){
   // 1 pixel row at a time
   current -= 1;
   // move the background with backgrond-position css properties
   $('section.bg-moving').css("backgroundPosition", (direction == 'v') ? current+"px 0" : "0 " + current+"px");
    }
    //Calls the scrolling function repeatedly
    setInterval("bgscroll()", scrollSpeed);



$("[data-toggle=popover]").popover('hide')
$(".show").popover('show')
$("[data-toggle=tooltip]").tooltip()
$(".show-tooltip").tooltip('show')
